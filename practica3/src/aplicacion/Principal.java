/*Copyright [2022] [Orianna Milone S.]

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package aplicacion;
import java.lang.Math;
import java.util.function.Supplier;
import java.util.Scanner;
import java.util.Collections;
import java.util.function.Function;
import java.lang.Object;
import java.util.Optional;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.*;
/**
 * @author Orianna Milone
 * 
 * Inicio del programa.
 * Primero se comprueba que el numero de tiros este comprendido en el rango pertinente.
 * Luego se implementa el metodo de montecarlo en varias partes. Haciendo uso de un metodo secundario asi mismo nombrado.
 * El programa consta solo de la clase principal. 
 */

public class Principal{
	public static void main(String args []){
		Scanner sc = new Scanner(System.in);
		System.out.println("Diga el numero de dardos a lanzar");
		double tiros = sc.nextDouble();

		if(tiros >= 100000000){
			System.out.println("Lo siento, debes intetar con un numero menor");
		}else{
			ArrayList<Double> puntos = new ArrayList();
			
			for(double i = 0; i <= tiros; i++){
				Double x = Math.random();
				Double y = Math.random();
				Double total = metodoMontecarlo(x, y, (z)-> z*z);
				puntos.add(total);
			}
			Long Dardos = puntos.stream()
				.filter(x -> x <= 1)	
				.count();
			System.out.println((4*Dardos)/tiros);
			}
	}

	public static Double metodoMontecarlo(Double x, Double y, Function<Double, Double> f){
		return f.apply(x) + f.apply(y);
	}
}
