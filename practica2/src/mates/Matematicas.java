/*Copyright [2022] [Orianna Milone S.]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package mates;
import java.lang.Math;

/**
 * @author Orianna Milone
 */

public class Matematicas{

	 /**
	 * Metodio para generar una aproximacion al numero pi, a traves del metodo de Montecarlo. Utilizando a la vez un metodo Recursivo.
	 * Toma como parametro la variable "i": que es un contador, "tiros":que son los dardos tirados, y "Dardos": inicializado siempre en 0, seran los dardos que coincidan con el area a evaluar. Los obtiene al ejecutar el programa desde consola.
	 * @param i
	 * @param tiros
	 * @param Dardos
         * @return Aproximacion al numero pi (a traves del metodo de Montecarlo.)
	 */

	public static double generarNumeroPiRecursivo(double i, double tiros, double Dardos){
		if(i <= tiros){
			double x = Math.random();
			double y = Math.random();
			double coordenada = (Math.pow(x,2)) + (Math.pow(y,2)); 
			if(coordenada <= 1){
				Dardos += 1;
			}
			return generarNumeroPiRecursivo(i + 1, tiros, Dardos);
		}else{
			return 4*Dardos/tiros;
		}
	}
}
