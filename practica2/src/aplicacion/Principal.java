/*Copyright [2022] [Orianna Milone S.]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package aplicacion;
import mates.Matematicas;
import java.util.Scanner;
	
/**
 * @author Orianna Milone
 * 
 * Inicio del programa.
 * Se hace llamado al metodo (Recursivo) que genera la aproximacion al numero pi, perteneciente a la clase "Matematicas".
 * Primero se comprueba que el numero de tiros este comprendido en el rango pertinente.
 */

public class Principal{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Diga el numero de dardos a lanzar");
		int tiros = sc.nextInt();
		if(tiros > 4000){
			System.out.println("Lo siento, debes intetar con un numero menor");
		}else{
			System.out.println("El número PI es " + Matematicas.generarNumeroPiRecursivo(1, tiros, 0));
		}
	}
}
