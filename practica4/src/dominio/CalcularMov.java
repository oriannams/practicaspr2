package dominio;
import java.util.*;

public class CalcularMov{

	public static int calcularmovimientos(String a, String b){
		int cambio = 0;
		BiFunction<Integer, Integer, Integer> minimo = (x,y) -> {
			if(x < y){
				return x;
			}else if(x > y){
				return y;
			}else{
				return x;
			}
		};

		a = "" + a;
		b = "" + b;

		String[] patron = a.split("");
		String[] cadena = b.split("");

		int [][] distancias = new int[cadena.length][patron.length];

		for(int i = 1; i < cadena.length; i++){
			for(int j = 1; j < patron.length; i++){
				if(patron[j].equalsIgnoreCase(cadena[j])){
					cambio = distancias[i-1][j-1];
				}else{
					cambio = 2 + distancias[i-1][j-1];
				}
				distancias[i][j] = minimo.apply(minimo.apply(1+distancias [i-1][j], 1+distancias[i][j-1], cambio));
			}					
		}
		return distancias[cadena.length-1][patron.length-1];
	}

	private static void inicializar(int [][] x, int cadena, int patron){
		for(int i = 0; i< cadena; i++){
			x[i][0] = i;
		}
		for(int j = 0; j< patron; j++){
			x[0][j] = j;
		}

	}


}

